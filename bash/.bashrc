#!/usr/bin/env bash
. "$HOME/.bashrc.$(uname)"

. "$HOME/.asdf/asdf.sh"

DOT="$HOME/src/gitlab.com/$USER/dotfiles"
dot() {
	case "$1" in
		"") make -C "$DOT" ;;
		*)  git -C "$DOT" "$@" ;;
	esac
}
export DOT

CONTAINER_HOST=ssh://docker
DOCKER_HOST=ssh://docker
export CONTAINER_HOST DOCKER_HOST
