#!/usr/bin/env bash
. "$HOME/.bashrc"

. "$HOME/.bash_profile.$(uname)"

. <(starship init bash)
. <(direnv hook bash) # should this maybe be in .bashrc?

. "$HOME/.asdf/completions/asdf.bash"
complete -C aws_completer aws

otp() {
    d="${PASSWORD_STORE_DIR:="$HOME/.password-store/"}"
    s="totp.gpg"
    pass otp -c "$(find "$d" -name "$s" | sed "s|^$d||" | sed "s|/$s$||" | fzf -1)/totp"
}

GPG_TTY="$(tty)"

VISUAL=nvim
EDITOR=nvim

alias vim=nvim

export GPG_TTY VISUAL EDITOR
# vi:ft=bash
