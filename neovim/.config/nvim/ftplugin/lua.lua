-- Set the 'path' so that 'includeexpr' works for `gf`.
vim.opt.path:append(vim.fn.fnamemodify(vim.env.MYVIMRC, ':h') .. '/lua')

-- <Leader>x executes the current line
vim.api.nvim_buf_set_keymap(0, 'n', '<Leader>x', '<Cmd>.lua<CR>', {
    desc = 'Execute the current line of lua',
    noremap = true,
    silent = true,
})

-- <Leader><Leader>x executes the current file
vim.api.nvim_buf_set_keymap(0, 'n', '<Leader><Leader>x', '<Cmd>source %<CR>', {
    desc = 'Source the current file',
    noremap = true,
    silent = true,
})

-- LSP setup
local nvim_runtime = vim.api.nvim_list_runtime_paths()
local workspace_folders = {}
for _, path in ipairs(nvim_runtime) do
    table.insert(workspace_folders, { uri = 'file://' .. path, name = path })
end
for _, path in ipairs(vim.api.nvim_get_runtime_file("*.lua", false)) do
    table.insert(workspace_folders, { uri = 'file://' .. path, name = path })
end

-- The global name for this LS
local lsp_name = 'lua-language-server'
-- The root for this buffer
local lsp_root = vim.fs.root(0, '.git')
-- Common settings for this LS, which includes the Neovim runtime paths
local lsp_settings = {
    Lua = {
        diagnostics = { globals = { 'vim' } },
        workspace = { library = nvim_runtime },
    }
}

vim.lsp.start({
    name = lsp_name,
    root_dir = lsp_root,
    cmd = { 'lua-language-server' },
    single_file_support = true,
    workspace_folders = workspace_folders,
    settings = lsp_settings,
    on_attach = function(client, bufnr)
        local nmap = function(desc, lhs, rhs)
            vim.keymap.set('n', lhs, rhs, {
                noremap = true,
                silent = true,
                buffer = bufnr,
                desc = desc,
            })
        end
        -- Check LSP capabilities and conditionally add key mappings
        if client.server_capabilities.definitionProvider then
            nmap('gd', 'buf.definition', 'List definitions')
        end
        if client.server_capabilities.hoverProvider then
            nmap('K', 'buf.hover', 'Context help')
        end
        if client.server_capabilities.renameProvider then
            nmap('<Leader>r', 'buf.rename', 'Rename')
        end
        if client.server_capabilities.referencesProvider then
            nmap('gr', 'buf.references', 'Get references')
        end
        if client.server_capabilities.documentHighlightProvider then
            nmap('[d', 'diagnostic.goto_prev', 'Previous diagnostic')
            nmap(']d', 'diagnostic.goto_next', 'Next diagnostic')
        end
        -- Auto-format on save
        if client.server_capabilities.documentFormattingProvider then
            vim.api.nvim_create_autocmd('BufWritePre', {
                group = vim.api.nvim_create_augroup('LspLuaFormatting', {
                    clear = true,
                }),
                buffer = bufnr,
                callback = function()
                    vim.lsp.buf.format({ async = false })
                end,
            })
        end
    end,
}, {
    bufnr = 0,
    reuse_client = function(_, config)
        return config.name == lsp_name and config.root_dir == lsp_root
    end,
})
