if vim.b.did_my_crystal_ftplugin then
    return
end
vim.b.did_my_crystal_ftplugin = true

-- Enable treesitter
require('nvim-treesitter.parsers').get_parser_configs().crystal = {
  install_info = {
    url = vim.env.HOME .. '/src/github.com/crystal-lang-tools/tree-sitter-crystal',
    branch = 'main',
    files = { 'src/parser.c', 'src/scanner.c' },
  },
  filetype = 'cr',
}

-- Format on save
vim.api.nvim_create_autocmd('BufWritePre', {
  pattern = '*.cr',
  callback = function()
    vim.cmd('CrystalFormat')
  end,
})

-- Run `crystal env` and parse its output
local crystal_env_output = vim.fn.system('crystal env')
if vim.v.shell_error ~= 0 then
  vim.notify('Failed to run `crystal env`', vim.log.levels.ERROR)
  return
end

-- Parse the output to extract environment variables
local crystal_path = nil
for line in crystal_env_output:gmatch('[^\r\n]+') do
  if line:match('^CRYSTAL_PATH=') then
    crystal_path = line:match('^CRYSTAL_PATH=(.*)')
    break
  end
end

-- Set `path` and `includeexpr` based on the parsed values
if crystal_path then
  -- Add Crystal library paths to `path` for file searching
  vim.opt_local.path:prepend(crystal_path)
  -- Add our local src path
  vim.opt_local.path:append('src')
  -- Set `includeexpr` to handle include paths
  vim.opt_local.includeexpr = 'v:fname'
else
  vim.notify(
    'CRYSTAL_PATH not found in `crystal env` output',
    vim.log.levels.WARN
  )
end

vim.opt_local.define = [[\v^\s*(class|module|def|macro)\s+\zs\w+\ze]]
vim.opt_local.include = [[\v^\s*require\s*]] .. '[' .. [[']] .. '"]\zs[^' .. [[']] .. '"]+\ze[' .. [[']] .. '"]'

local function run_crystal_specs()
  if vim.bo.filetype ~= 'crystal' then
    return
  end
  vim.notify('TODO: run crystal spec')
end

vim.api.nvim_create_user_command('CrystalRunSpec', run_crystal_specs, {})

