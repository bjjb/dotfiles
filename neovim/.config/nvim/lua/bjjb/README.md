# bjjb's Neovim Configuration

To use this, set the whole contents of your `$XDG_CONFIG/nvim/init.lua` to:

```lua
require('bjjb')
```
