-- [[ bjjb's nvim config module ]] --
require('bjjb.options')
require('bjjb.packages')
require('bjjb.autocommands')
require('bjjb.keymaps')
require('bjjb.appearance')
