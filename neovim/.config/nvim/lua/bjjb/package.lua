-- A super simple package "manager". It really just clones repositories into
-- the optional package path in the site dir and adds them.
-- TODO: a vimscript version (coz it should work fine in vim).

local opt = vim.fn.stdpath('data') .. '/site/pack/%s/opt/%s'

-- Installs a package by (git cloning from a HTTP remote) unless it already
-- exists. Packages must have 2 or 3 path components - in the former case,
-- 'github.com' is assumed. Additional path components are ignored currently.
local install = function(packages)
  if type(packages) == 'string' then
    packages = { packages }
  end
  -- if we're given a list, install all the packages
  for _, package in ipairs(packages) do
    local parts = vim.split(package, '/')
    local host = parts[#parts - 2] or 'github.com'
    local owner = parts[#parts - 1]
    local repo = parts[#parts]
    local dest = string.format(opt, owner, repo)
    if vim.fn.isdirectory(dest .. '/' .. '.git') == 0 then
      vim.notify('Installing ' .. package, vim.log.levels.INFO)
      vim.fn.mkdir(vim.fn.fnamemodify(dest, ':h'), 'p')
      vim.fn.system({
        'git',
        '-C', vim.fn.fnamemodify(dest, ':h'),
        'clone',
        '--filter=blob:none',
        string.format('https://%s/%s/%s', host, owner, repo),
      })
      if vim.v.shell_error ~= 0 then
        vim.notify('Error cloning ' .. package, vim.log.levels.ERROR)
      end
    end
  end
  return packages
end

-- Ensures a package is installed, adds it to the runtimepath, and tags its
-- documentation.
-- TODO: maybe allow use of packadd! with an option
-- TODO: maybe allow a callback function
local add = function(packages)
  if type(packages) == 'string' then package = { packages } end
  for _, package in ipairs(packages) do
    install(package)
    vim.cmd('packadd ' .. vim.fn.fnamemodify(package, ':t'))
  end
  vim.cmd.helptags('ALL') -- TODO more specific helptags
end

-- TODO
local list = function()
  vim.notify('not implemented', vim.log.levels.WARNING)
end

-- TODO
local remove = function(packages)
  vim.notify('not implemented', vim.log.levels.WARNING)
end

return {
  install = install,
  add     = add,
  list    = list,
  remove  = remove,
}
