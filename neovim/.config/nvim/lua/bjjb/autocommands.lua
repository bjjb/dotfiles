vim.api.nvim_create_autocmd('TextYankPost', {
  desc = 'Highlight when yanking (copying) text',
  group = vim.api.nvim_create_augroup('YankHighlight', { clear = true }),
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- [[ Set wrap and spell for markdown/gitcommit files ]] --
vim.api.nvim_create_autocmd('FileType', {
  desc = 'Set wrap and spell on for particular filetypes',
  group = vim.api.nvim_create_augroup('WrapSpell', { clear = true }),
  pattern = { 'markdown', 'text', 'gitcommit' },
  callback = function()
    vim.opt_local.wrap = true
    vim.opt_local.spell = true
  end,
})
