--[[
Common options for all filetypes and buffers.
This file is essentially just used for setting options. For autocommands,
mappings, etc, see the other files in this package.
--]]

local set = vim.opt

-- show numbers and a sign column
set.number = true
set.numberwidth = 3
set.signcolumn = 'yes:1'
-- allow the mouse to perform some actions
set.mouse = 'a'
-- Decrease update time
set.updatetime = 250
-- Decrease mapped sequence wait time
set.timeoutlen = 300
-- Enable break indent
set.breakindent = true
-- Save undo history
set.undofile = true
-- Case-insensitive searching (unless the search term is mixed-case)
set.ignorecase = true
set.smartcase = true
-- Sets how neovim will display certain whitespace characters
set.list = true
set.listchars = { tab = '» ', trail = '·', nbsp = '␣' }
-- Minimal number of screen lines to keep above and below the cursor.
set.scrolloff = 5
-- Preview substitutions live
set.inccommand = 'nosplit'
-- Show which line your cursor is on and the 80th column
set.cursorline = true
set.colorcolumn = '80'
-- Default shift-width
set.shiftwidth = 4
set.smartindent = true
-- Enable search/replace preview
set.incsearch = true
