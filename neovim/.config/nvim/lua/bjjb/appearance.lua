-- [[ Default colorscheme ]] --
vim.cmd('colo habamax')

-- [[ Don't paint the background in the terminal (or GUI) ]]--
vim.cmd('hi Normal ctermbg=none guibg=none')

-- [[ Some plugins use this to see how to render certain glyphs ]] --
vim.g.have_nerd_font = true
