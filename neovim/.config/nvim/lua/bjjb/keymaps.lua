--  See `:help 'hls`
vim.keymap.set('n', '<Esc>', '<cmd>nohls<CR>', { desc = 'Search-highlight off' })
-- Exit terminal mode in the builtin terminal with a shortcut that is a bit
-- easier for people to discover. Otherwise, you normally need to press
-- <C-\><C-n>, which is not what someone will guess without a bit more
-- experience.
-- See `:help Terminal-mode`
vim.keymap.set('t', '<Esc><Esc>', '<C-\\><C-n>', { desc = 'Normal mode' })
