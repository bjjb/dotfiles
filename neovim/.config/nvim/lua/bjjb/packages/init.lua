-- Ensure each of these is installed into a pack/opt/ directory in the standard
-- site path which is then added to the runtimepath (with its documentation
-- tagged).
require('bjjb.package').add({
	'nvim-lua/plenary.nvim', -- Lua functions (used by other packages)
	'nvimtools/none-ls.nvim', -- LSP features for some language tools
	'tpope/vim-abolish',     -- Coerce common patterns
	'tpope/vim-apathy',      -- Detect typical paths
	'tpope/vim-commentary',  -- Work with comments
	'tpope/vim-dispatch',    -- Asynchronously run tasks
	'tpope/vim-endwise',     -- Detect blocks
	'tpope/vim-eunuch',      -- UNIX commands
	'tpope/vim-fugitive',    -- Work with Git
	'tpope/vim-projectionist', -- Work with project definitions
	'tpope/vim-repeat',      -- Repeat more commands
	'tpope/vim-sleuth',      -- Detect tabstop and shiftwidth automatically
	'tpope/vim-surround',    -- Better motion surround commands
	'tpope/vim-unimpaired',  -- Typical bracket commands
	'tpope/vim-dadbod',      -- Work with databases
	'kristijanhusak/vim-dadbod', -- Nice DB integration
	'kristijanhusak/vim-dadbod-ui', -- A UI for vim-dadbod
	'lewis6991/gitsigns.nvim', -- Git indicators in the gutter
	'folke/trouble.nvim',    -- Pretty diagnostics, references, etc
	'folke/todo-comments.nvim', -- Highlighted TODO comments
	'vim-crystal/vim-crystal', -- Crystal syntax/commands
	'pablos123/shellcheck.nvim', -- Shellcheck for shell-scripts
	'hashivim/vim-terraform', -- Terraform support
})

-- [[ Configure treesitter ]] --
require('bjjb.packages.treesitter')
