require('bjjb.package').add({ 'nvim-treesitter/nvim-treesitter' })

require('nvim-treesitter.configs').setup({
  ensure_installed = {
    'lua', 'vim', 'go', 'javascript', 'typescript', 'regex', 'bash', 'perl',
    'python', 'ruby', 'terraform', 'hcl', 'sql', 'markdown', 'markdown_inline',
    'rust', 'erlang', 'elixir', 'gleam', 'java', 'kotlin', 'clojure', 'groovy',
    'c', 'cpp', 'objc'
  },
  highlight = { enable = true },
  indent = { enable = true },
  incremental_selection = { enable = true },
  auto_install = true,
})
