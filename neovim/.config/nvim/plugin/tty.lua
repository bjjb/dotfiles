-- Function to create a floating terminal
local state = { tty = { buf = -1, win = -1 } }

local function tty(opts)
  local width = opts.width or math.floor(vim.o.columns * 0.8)
  local height = opts.height or math.floor(vim.o.lines * 0.8)
  local col = math.floor((vim.o.columns - width) / 2)
  local row = math.floor((vim.o.lines - height) / 2)

  local buf = nil
  if vim.api.nvim_buf_is_valid(opts.buf) then
    buf = opts.buf
  else
    buf = vim.api.nvim_create_buf(false, true) -- no file, scratch
  end

  local config = {
    relative = 'editor',
    height = height,
    width = width,
    col = col,
    row = row,
    style = 'minimal',
    border = 'rounded',
  }

  local win = vim.api.nvim_open_win(buf, true, config)

  return { buf = buf, win = win }
end

vim.api.nvim_set_hl(0, 'FloatBorder', { fg = '#ffffff', bg = 'none' })
vim.api.nvim_set_hl(0, 'NormalFloat', { fg = '#ffffff', bg = 'none' })

local function toggle_tty()
  if not vim.api.nvim_win_is_valid(state.tty.win) then
    state.tty = tty({ buf = state.tty.buf })
    if vim.bo[state.tty.buf].buftype ~= 'terminal' then
      -- Open the terminal with a specific shell (e.g., bash)
      vim.fn.termopen('bash -l', {
        on_exit = function()
          vim.api.nvim_buf_delete(state.tty.buf, { force = true })
        end
      })
      vim.api.nvim_buf_set_keymap(state.tty.buf, 'n', '<Esc>', '<Cmd>TTY<CR>', {
      })
    end
    -- Start the terminal in insert mode
    vim.api.nvim_command('startinsert')
  else
    vim.api.nvim_win_hide(state.tty.win)
  end
end

vim.api.nvim_create_user_command("TTY", toggle_tty, {
  desc = "Open a floating terminal"
})

vim.api.nvim_set_keymap('n', '<Leader><CR>', '<Cmd>TTY<CR>', {
  noremap = true,
  silent = true,
})
