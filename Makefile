.PHONY: all clobber

# To see more info from stow, use
#
#	VERBOSE=vv make
VERBOSE ?=

all:
	stow -$(VERBOSE)Rt $(HOME) */

clobber:
	stow -$(VERBOSE)Dt $(HOME) */
