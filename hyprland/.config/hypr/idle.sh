#!/bin/sh

# To use this script, add it as a wayidle timeout to your hyprland.conf, e.g.
#
#	exec-once = ~/.config/hypr/idle.sh 300

t="${1:-300}"

# Only take action after an idle timeout (i.e., not the first time). If there's
# a second argument, switch off the display (dpms off) and start hyprlock
# unless it's already running. Hyprland should be configured to turn on dpms on
# a key-press (or mouse-movement).
[ -n "$2" ] \
    && hyprctl dispatch dpms off \
    && [ pgrep -q hyprlock ] || hyprctl dispatch exec hyprlock

# Replace this process with wayidle which will call this script on time-out
exec wayidle -t "$t" hyprctl dispatch exec "$0" "$t" "off"
