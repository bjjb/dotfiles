"
" bjjb's .vimrc
"

" Ensure directories exist for swapfiles, backups and the state file
sil exe '!mkdir -p ~/.cache/vim/swp'
sil exe '!mkdir -p ~/.cache/vim/backup'
sil exe '!mkdir -p ~/.local/state'

se nocp
se bs=indent,eol,start
se tgc mouse=a
se noeb vb
se hid ar
se udf udir=~/.cache/vim/undo
se dir=~/.cache/vim/swp//
se bdir=~/.cache/vim/backup//
se vif=~/.local/state/vim
se so=4 siso=5
se hls is scs
se ml ex
se wmnu ic
se nu nuw=4 scl=yes
se nosc nosmd noru
se smc=256
se bg=dark cc=80
se sts=4 sw=4 noet
se ut=400 bdlay=200

syn on

filet plugin indent on

ru macros/matchit.vim

packl " should be a no-op; I prefer to `git submodule add` packages into
      " pack/<author>opt/ and :packadd them explicitly.
pa abolish " word substitution helper
pa apathy " auto-sets 'pa', 'sua', 'inc', 'inex' and 'dev' for some files
pa commentary " adds gc/gC to (un)comment based on 'cms'
pa dadbod " database console
pa dispatch " async job runner
pa endwise " types 'end', 'endwhile', etc, for you, where needed
pa eunuch " vim commands for common UNIX utilities
pa fugitive " git integration
pa repeat " makes . usable by plugins
pa projectionist " project-specific settings
pa surround " convenient syntax for brackets, tags, etc
pa unimpaired " navigation shortcuts with [ and ]
pa editorconfig " honour .editorconfig files
pa crystal " Crystal language support
pa golang " Go language support
pa terraform " Terraform/OpenTofu language support
pa emmet " HTML/CSS snippets
pa mustache " Mustache/Handlebars

" Defines common digraphs/emojis
exe 'dig :x 128544'
exe 'dig :) 128578'
exe 'dig :D 128512'
exe 'dig =D 128513'
exe 'dig :P 128523'
exe 'dig :O 128558'
exe 'dig :/ 128533'
exe 'dig :( 128577'
exe 'dig =( 128543'
exe 'dig :E 128556'
exe 'dig /\ 128591'
exe 'dig \/ 129335'
exe 'dig :\| 128528'
exe 'dig =\| 128529'
exe 'dig -k 128070'
exe 'dig -j 128071'
exe 'dig -h 128072'
exe 'dig -l 128073'
exe 'dig [b 128161'
exe 'dig [w 128075'
exe 'dig [! 128163'

nn <Esc><Esc> :setl invhls<CR>
nn <Leader>S  :up<CR>:so %<CR>
nn <Leader>W  :setl invspell<CR>
nn <Leader>L  :setl invlist<CR>

colo habamax
hi Normal ctermbg=NONE guibg=NONE
