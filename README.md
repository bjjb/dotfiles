# bjjb's dotfiles

Contains some configuration files for use on [FreeBSD][], [Linux][] and MacOS.

Generally, I work in [vim][] or [neovim][], and run commands in [bash][], often
in a [tmux][] session, with a prompt configured with [starship][]. When working
in a graphical environment (typically [hyprland][]), I mostly use [alacritty][]
(with the [hack][] font) and [librewolf][].

Other tools I use a lot are [git][], [asdf][] (to manage programming languages
and other tools), [kubectl][], [gpg][] and [pass][].

I keep things up to date by cloning the repo (typically to
~/src/gitlab.com/bjjb/dotfiles) and using [stow][] from there (or just running
`make`).

![A typical desktop session on FreeBSD](screenshot.jpg)

I like to use [xdg][] directory standards, but a lot of software does not (vim,
bash or asdf, for example). Thus for bash, there's a `~/.config/bash` folder
which the `~/.bash_profile` sources on login, allowing me to keep shell setup
alongside the rest of the package's config.

[FreeBSD]: https://www.freebsd.org/
[Linux]: https://kernel.org/
[vim]: https://www.vim.org
[neovim]: https://neovim.io
[bash]: https://www.gnu.org/software/bash/
[tmux]: https://tmux.github.io/
[starship]: https://starship.rs/
[hyprland]: https://hyprland.org/
[alacritty]: https://alacritty.org/
[hack]: https://sourcefoundry.org/hack/
[librewolf]: https://librewolf.net/
[git]: https://git-scm.com/
[asdf]: https://asdf-vm.com/
[kubectl]: https://kubernetes.io/docs/reference/kubectl/
[gpg]: https://gnupg.org/
[pass]: https://www.passwordstore.org/
[stow]: https://www.gnu.org/software/stow/
[xdg]: https://specifications.freedesktop.org/basedir-spec/latest/
